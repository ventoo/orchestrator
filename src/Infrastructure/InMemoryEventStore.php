<?php

namespace Util\Orchestrator\Infrastructure;

use Util\Orchestrator\Application\EventStore;
use function array_merge;

class InMemoryEventStore implements EventStore
{
    private $persistence;

    public function __construct()
    {
        $this->persistence = [];
    }

    public function load(string $id): iterable
    {
        return $this->persistence[$id] ?? [];
    }

    public function append(string $id, iterable $events): void
    {
        $this->persistence[$id] = array_merge($this->persistence[$id] ?? $this->persistence[$id] = [], $events);
    }
}
