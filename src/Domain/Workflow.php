<?php

namespace Util\Orchestrator\Domain;

use Generator;
use ReflectionClass;

interface Workflow
{
    public function run(OrchestrationContext $context, ...$input): Generator;
}
