<?php

namespace Util\Orchestrator\Domain;

use InvalidArgumentException;
use function ctype_xdigit;
use function strlen;

class ExecutionId
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id)
    {
        $this->setId($id);
    }

    public function __toString()
    {
        return $this->id;
    }

    public function id(): string
    {
        return $this->id;
    }

    private function setId(string $id): void
    {
        if (ctype_xdigit($id) === false || strlen($id) !== 32) {
            throw new InvalidArgumentException('Invalid value');
        }
        $this->id = $id;
    }
}
