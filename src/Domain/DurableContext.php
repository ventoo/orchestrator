<?php

namespace Util\Orchestrator\Domain;

use Util\Orchestrator\Domain\Event\ExecutionCompleted;
use Util\Orchestrator\Domain\Event\ExecutionFaulted;
use Util\Orchestrator\Domain\Event\ExecutionStarted;
use Util\Orchestrator\Domain\Event\TaskCanceled;
use Util\Orchestrator\Domain\Event\TaskCompleted;
use Util\Orchestrator\Domain\Event\TaskFaulted;
use Util\Orchestrator\Domain\Event\TaskScheduled;
use Util\Pubsub\Event;
use InvalidArgumentException;
use RuntimeException;
use function array_map;
use function count;
use function serialize;

class DurableContext implements OrchestrationContext
{
    private const STATUS_CREATED = 'created';
    private const STATUS_STARTED = 'started';
    private const STATUS_COMPLETED = 'completed';
    private const STATUS_FAULTED = 'faulted';

    /**
     * @var string
     */
    private $status;

    /**
     * @var ExecutionId
     */
    private $executionId;

    private $changes = [];

    private $taskId = -1;

    private $tasks = [];

    /**
     * @var string
     */
    private $workflowName;

    /**
     * @var null|array
     */
    private $input;

    /**
     * @var null|array
     */
    private $result;

    /**
     * @var null|string
     */
    private $exception;

    /**
     * @param ExecutionId $executionId
     * @param Event[] $events
     */
    public function __construct(ExecutionId $executionId, iterable $events)
    {
        $this->setStatus(self::STATUS_CREATED);
        $this->setExecutionId($executionId);
        foreach ($events as $event) {
            $this->mutate($event);
        }
    }

    public function runTask(string $operation, ...$input): int
    {
        if ('whenAny' === $operation
            || 'whenAll' === $operation
        ) {
            throw new RuntimeException('Operations \'whenAny\', \'whenAll\' are reserved');
        }
        if ($this->task($this->nextId()) === null) {
            $this->apply(
                new TaskScheduled(
                    $this->executionId,
                    $this->taskId,
                    $operation,
                    $input
                )
            );
        }

        return $this->taskId;
    }

    public function whenAny(int ...$taskIds): int
    {
        if ($this->task($this->nextId()) === null) {
            $this->apply(
                new TaskScheduled(
                    $this->executionId,
                    $this->taskId,
                    'whenAny',
                    $taskIds
                )
            );
        }
        $exception = [];
        foreach ($taskIds as $taskId) {
            $task = $this->nonNullTask($taskId);
            if ($task->isCompletedSuccessfully()) {
                $this->completeTask($this->taskId, serialize([$task->id(), $task->result()]));
                break;
            } elseif ($task->isFaulted()) {
                $exception[] = [$task->id(), $task->exception()];
            }
        }
        if (count($exception) === count($taskIds)) {
            $this->failTask($this->taskId, '');
        }

        return $this->taskId;
    }

    public function whenAll(int ...$taskIds): int
    {
        if ($this->task($this->nextId()) === null) {
            $this->apply(
                new TaskScheduled(
                    $this->executionId,
                    $this->taskId,
                    'whenAll',
                    $taskIds
                )
            );
        }
        $result = [];
        foreach ($taskIds as $taskId) {
            $task = $this->nonNullTask($taskId);
            if ($task->isScheduled()) {
                break;
            } elseif ($task->isFaulted()) {
                $this->failTask($this->taskId, $task->exception());
                break;
            } else {
                $result[] = [$task->id(), $task->result()];
            }
        }
        if (count($result) === count($taskIds)) {
            $this->completeTask($this->taskId, serialize($result));
        }

        return $this->taskId;
    }

    public function task(int $taskId): ?Task
    {
        return $this->tasks[$taskId] ?? null;
    }

    /**
     * @return iterable&Event[]
     */
    public function changes(): iterable
    {
        return $this->changes;
    }

    public function startExecution(string $workflowName, array $input): void
    {
        if (!$this->isCreated()) {
            return;
        }
        $this->apply(
            new ExecutionStarted(
                $this->executionId,
                $workflowName,
                $input
            )
        );
    }

    public function workflowName(): string
    {
        return $this->workflowName;
    }

    public function input(): ?array
    {
        return $this->input;
    }

    public function completeExecution(): void
    {
        if (!$this->isStarted()) {
            return;
        }
        $this->apply(
            new ExecutionCompleted(
                $this->executionId,
                array_map(
                    function (Task $task) {
                        return $task->result();
                    },
                    $this->tasks
                )
            )
        );
    }

    public function result(): ?array
    {
        return $this->result;
    }

    public function failExecution(string $exception): void
    {
        if (!$this->isStarted()) {
            return;
        }
        $this->apply(
            new ExecutionFaulted(
                $this->executionId,
                $exception
            )
        );
    }

    public function exception(): ?string
    {
        return $this->exception;
    }

    public function completeTask(int $taskId, string $result): void
    {
        $task = $this->nonNullTask($taskId);
        if (!$task->isScheduled()) {
            return;
        }
        $this->apply(
            new TaskCompleted(
                $this->executionId,
                $task->id(),
                $task->operation(),
                $result
            )
        );
    }

    public function failTask(int $taskId, string $exception): void
    {
        $task = $this->nonNullTask($taskId);
        if (!$task->isScheduled()) {
            return;
        }

        $this->apply(
            new TaskFaulted(
                $this->executionId,
                $task->id(),
                $task->operation(),
                $exception
            )
        );
    }

    public function cancelTask(int $taskId, string $cancellation): void
    {
        $task = $this->nonNullTask($taskId);
        if (!$task->isScheduled()) {
            return;
        }
        $this->apply(
            new TaskCanceled(
                $this->executionId,
                $task->id(),
                $task->operation(),
                $cancellation
            )
        );
    }

    public function isCreated(): bool
    {
        return self::STATUS_CREATED === $this->status;
    }

    public function isStarted(): bool
    {
        return self::STATUS_STARTED === $this->status;
    }

    public function isCompletedSuccessfully(): bool
    {
        return self::STATUS_COMPLETED === $this->status;
    }

    public function isFaulted(): bool
    {
        return self::STATUS_FAULTED === $this->status;
    }

    public function isCompleted(): bool
    {
        return $this->isCompletedSuccessfully() || $this->isFaulted();
    }

    private function setStatus(string $status): void
    {
        $this->status = $status;
    }

    private function setExecutionId(ExecutionId $executionId): void
    {
        $this->executionId = $executionId;
    }

    private function setWorkflowName(string $workflowName): void
    {
        $this->workflowName = $workflowName;
    }

    private function setInput(array $input): void
    {
        $this->input = $input;
    }

    private function setResult(array $result): void
    {
        $this->result = $result;
    }

    private function setException(string $exception): void
    {
        $this->exception = $exception;
    }

    private function mutate(Event $event): void
    {
        switch ($event->type()) {
            case ExecutionStarted::class:
                /** @var ExecutionStarted $event */
                $this->setStatus(self::STATUS_STARTED);
                $this->setWorkflowName($event->workflowName());
                $this->setInput($event->input());
                break;
            case ExecutionCompleted::class:
                /** @var ExecutionCompleted $event */
                $this->setStatus(self::STATUS_COMPLETED);
                $this->setResult($event->result());
                break;
            case ExecutionFaulted::class:
                /** @var ExecutionFaulted $event */
                $this->setStatus(self::STATUS_FAULTED);
                $this->setException($event->exception());
                break;
            case TaskScheduled::class:
                /** @var TaskScheduled $event @phpstan-ignore-next-line */
                $task = $this->newTask($event->taskId(), $event->operation());
                $this->tasks[$task->id()] = $task->fromInput($event->input());
                break;
            case TaskCompleted::class:
                /** @var TaskCompleted $event @phpstan-ignore-next-line */
                $task = $this->nonNullTask($event->taskId());
                $this->tasks[$task->id()] = $task->fromResult($event->result());
                break;
            case TaskFaulted::class:
                /** @var TaskFaulted $event @phpstan-ignore-next-line*/
                $task = $this->nonNullTask($event->taskId());
                $this->tasks[$task->id()] = $task->fromException($event->exception());
                break;
            case TaskCanceled::class:
                /** @var TaskCanceled $event @phpstan-ignore-next-line*/
                $task = $this->nonNullTask($event->taskId());
                $this->tasks[$task->id()] = $task->fromCanceled($event->cancellation());
                break;
            default:
                break;
        }
    }

    private function apply(Event $event): void
    {
        $this->changes[] = $event;
        $this->mutate($event);
    }

    private function nextId(): int
    {
        return ++$this->taskId;
    }

    private function nonNullTask(int $taskId): Task
    {
        $task = $this->task($taskId);
        if (null === $task) {
            throw new InvalidArgumentException('Task does not exist');
        }

        return $task;
    }

    private function newTask(int $taskId, string $operation): Task
    {
        return new Task($taskId, $operation);
    }
}
