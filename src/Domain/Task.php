<?php

namespace Util\Orchestrator\Domain;

use InvalidArgumentException;
use function in_array;

class Task
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $operation;

    /**
     * @var string
     */
    private $status;

    /**
     * @var array
     */
    private $input;

    /**
     * @var null|string
     */
    private $result;

    /**
     * @var null|string
     */
    private $exception;

    /**
     * @var null|string
     */
    private $cancellation;

    private const STATUS_CREATED = 'created';
    private const STATUS_FAULTED = 'faulted';
    private const STATUS_SCHEDULED = 'scheduled';
    private const STATUS_COMPLETED = 'completed';
    private const STATUS_CANCELED = 'canceled';

    public function __construct(int $id, string $operation)
    {
        $this->setId($id);
        $this->setOperation($operation);
        $this->setStatus(self::STATUS_CREATED);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function operation(): string
    {
        return $this->operation;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function input(): array
    {
        return $this->input;
    }

    public function result(): ?string
    {
        return $this->result;
    }

    public function exception(): ?string
    {
        return $this->exception;
    }

    public function cancellation(): ?string
    {
        return $this->cancellation;
    }

    public function fromInput(array $input): self
    {
        $clone = clone $this;
        $clone->setStatus(self::STATUS_SCHEDULED);
        $clone->setInput($input);

        return $clone;
    }

    public function fromResult(string $result): self
    {
        $clone = clone $this;
        $clone->setStatus(self::STATUS_COMPLETED);
        $clone->setResult($result);

        return $clone;
    }

    public function fromException(string $exception): self
    {
        $clone = clone $this;
        $clone->setStatus(self::STATUS_FAULTED);
        $clone->setException($exception);

        return $clone;
    }

    public function fromCanceled(string $cancellation): self
    {
        $clone = clone $this;
        $clone->setStatus(self::STATUS_CANCELED);
        $clone->setCancellation($cancellation);

        return $clone;
    }

    public function isCreated(): bool
    {
        return self::STATUS_CREATED === $this->status;
    }

    public function isFaulted(): bool
    {
        return self::STATUS_FAULTED === $this->status;
    }

    public function isScheduled(): bool
    {
        return self::STATUS_SCHEDULED === $this->status;
    }

    public function isCompletedSuccessfully(): bool
    {
        return self::STATUS_COMPLETED === $this->status;
    }

    public function isCanceled(): bool
    {
        return self::STATUS_CANCELED === $this->status;
    }

    public function isCompleted(): bool
    {
        return $this->isCompletedSuccessfully() || $this->isCanceled() || $this->isFaulted();
    }

    private function setId(int $id): void
    {
        if ($id < 0) {
            throw new InvalidArgumentException('Id may not be less than 0');
        }
        $this->id = $id;
    }

    private function setStatus(string $status): void
    {
        $statuses = [
            self::STATUS_CREATED,
            self::STATUS_FAULTED,
            self::STATUS_SCHEDULED,
            self::STATUS_COMPLETED,
            self::STATUS_CANCELED
        ];
        if (!in_array($status, $statuses)) {
            throw new InvalidArgumentException('Status takes an invalid value');
        }
        $this->status = $status;
    }

    private function setOperation(string $operation): void
    {
        if ('' === $operation) {
            throw new InvalidArgumentException('Operation may not be empty');
        }
        $this->operation = $operation;
    }

    private function setInput(array $input): void
    {
        $this->input = $input;
    }

    private function setResult(string $result): void
    {
        $this->result = $result;
    }

    private function setException(string $exception): void
    {
        $this->exception = $exception;
    }

    private function setCancellation(string $cancellation): void
    {
        $this->cancellation = $cancellation;
    }
}
