<?php

namespace Util\Orchestrator\Domain\Event;

use Util\Pubsub\Event;
use DateTimeImmutable;

class ExecutionFaulted extends Event
{
    /**
     * @var DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @var string
     */
    private $executionId;

    /**
     * @var string
     */
    private $exception;

    public function __construct(string $executionId, string $exception)
    {
        $this->setOccurredOn(new DateTimeImmutable());
        $this->setExecutionId($executionId);
        $this->setException($exception);
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function exception(): string
    {
        return $this->exception;
    }

    private function setOccurredOn(DateTimeImmutable $occurredOn): void
    {
        $this->occurredOn = $occurredOn;
    }

    private function setExecutionId(string $executionId): void
    {
        $this->executionId = $executionId;
    }

    private function setException(string $exception): void
    {
        $this->exception = $exception;
    }
}
