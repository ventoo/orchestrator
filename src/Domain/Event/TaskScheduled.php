<?php

namespace Util\Orchestrator\Domain\Event;

use Util\Pubsub\Event;
use DateTimeImmutable;

class TaskScheduled extends Event
{
    /**
     * @var DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @var string
     */
    private $executionId;

    /**
     * @var int
     */
    private $taskId;

    /**
     * @var string
     */
    private $operation;

    /**
     * @var array
     */
    private $input;

    public function __construct(string $executionId, int $taskId, string $operation, array $input)
    {
        $this->setOccurredOn(new DateTimeImmutable());
        $this->setExecutionId($executionId);
        $this->setTaskId($taskId);
        $this->setOperation($operation);
        $this->setInput($input);
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function taskId(): int
    {
        return $this->taskId;
    }

    public function operation(): string
    {
        return $this->operation;
    }

    public function input(): array
    {
        return $this->input;
    }

    private function setOccurredOn(DateTimeImmutable $occurredOn): void
    {
        $this->occurredOn = $occurredOn;
    }

    private function setExecutionId(string $executionId): void
    {
        $this->executionId = $executionId;
    }

    private function setTaskId(int $taskId): void
    {
        $this->taskId = $taskId;
    }

    private function setOperation(string $operation): void
    {
        $this->operation = $operation;
    }

    private function setInput(array $input): void
    {
        $this->input = $input;
    }
}
