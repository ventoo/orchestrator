<?php

namespace Util\Orchestrator\Domain\Event;

use Util\Pubsub\Event;
use DateTimeImmutable;

class ExecutionCompleted extends Event
{
    /**
     * @var DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @var string
     */
    private $executionId;

    /**
     * @var array
     */
    private $result;

    public function __construct(string $executionId, array $result)
    {
        $this->setOccurredOn(new DateTimeImmutable());
        $this->setExecutionId($executionId);
        $this->setResult($result);
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function result(): array
    {
        return $this->result;
    }

    private function setOccurredOn(DateTimeImmutable $occurredOn): void
    {
        $this->occurredOn = $occurredOn;
    }

    private function setExecutionId(string $executionId): void
    {
        $this->executionId = $executionId;
    }

    private function setResult(array $result): void
    {
        $this->result = $result;
    }
}
