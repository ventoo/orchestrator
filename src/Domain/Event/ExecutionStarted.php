<?php

namespace Util\Orchestrator\Domain\Event;

use Util\Pubsub\Event;
use DateTimeImmutable;

class ExecutionStarted extends Event
{
    /**
     * @var DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @var string
     */
    private $executionId;

    /**
     * @var string
     */
    private $workflowName;

    /**
     * @var array
     */
    private $input;

    public function __construct(string $executionId, string $workflowName, array $input)
    {
        $this->setOccurredOn(new DateTimeImmutable());
        $this->setExecutionId($executionId);
        $this->setWorkflowName($workflowName);
        $this->setInput($input);
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function workflowName(): string
    {
        return $this->workflowName;
    }

    public function input(): array
    {
        return $this->input;
    }

    private function setOccurredOn(DateTimeImmutable $occurredOn): void
    {
        $this->occurredOn = $occurredOn;
    }

    private function setExecutionId(string $executionId): void
    {
        $this->executionId = $executionId;
    }

    private function setWorkflowName(string $workflowName): void
    {
        $this->workflowName = $workflowName;
    }

    private function setInput(array $input): void
    {
        $this->input = $input;
    }
}
