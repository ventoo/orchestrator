<?php

namespace Util\Orchestrator\Domain\Event;

use Util\Pubsub\Event;
use DateTimeImmutable;

class TaskCompleted extends Event
{
    /**
     * @var DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @var string
     */
    private $executionId;

    /**
     * @var int
     */
    private $taskId;

    /**
     * @var string
     */
    private $operation;

    /**
     * @var string
     */
    private $result;

    public function __construct(string $executionId, int $taskId, string $operation, string $result)
    {
        $this->setOccurredOn(new DateTimeImmutable());
        $this->setExecutionId($executionId);
        $this->setTaskId($taskId);
        $this->setOperation($operation);
        $this->setResult($result);
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function taskId(): int
    {
        return $this->taskId;
    }

    public function operation(): string
    {
        return $this->operation;
    }

    public function result(): string
    {
        return $this->result;
    }

    private function setOccurredOn(DateTimeImmutable $occurredOn): void
    {
        $this->occurredOn = $occurredOn;
    }

    private function setExecutionId(string $executionId): void
    {
        $this->executionId = $executionId;
    }

    private function setTaskId(int $taskId): void
    {
        $this->taskId = $taskId;
    }

    private function setOperation(string $operation): void
    {
        $this->operation = $operation;
    }

    private function setResult(string $result): void
    {
        $this->result = $result;
    }
}
