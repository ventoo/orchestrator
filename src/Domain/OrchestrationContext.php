<?php

namespace Util\Orchestrator\Domain;

interface OrchestrationContext
{
    public function runTask(string $operation, ...$input): int;

    public function whenAny(int ...$taskIds): int;

    public function whenAll(int ...$taskIds): int;
}
