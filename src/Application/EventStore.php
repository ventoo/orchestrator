<?php

namespace Util\Orchestrator\Application;

interface EventStore
{
    public function load(string $id): iterable;

    public function append(string $id, iterable $events): void;
}
