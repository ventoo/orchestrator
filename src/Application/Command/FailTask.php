<?php

namespace Util\Orchestrator\Application\Command;

use InvalidArgumentException;

class FailTask
{
    /**
     * @var string
     */
    private $executionId;

    /**
     * @var int
     */
    private $taskId;

    /**
     * @var string
     */
    private $exception;

    public function __construct(string $executionId, int $taskId, string $exception)
    {
        $this->setExecutionId($executionId);
        $this->setTaskId($taskId);
        $this->setException($exception);
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function taskId(): int
    {
        return $this->taskId;
    }

    public function exception(): string
    {
        return $this->exception;
    }

    private function setExecutionId(string $executionId): void
    {
        if ('' === $executionId) {
            throw new InvalidArgumentException('ExecutionId may not be empty');
        }
        $this->executionId = $executionId;
    }

    private function setTaskId(int $taskId): void
    {
        if ($taskId < 0) {
            throw new InvalidArgumentException('TaskId may not be less than zero');
        }
        $this->taskId = $taskId;
    }

    private function setException(string $exception): void
    {
        $this->exception = $exception;
    }
}
