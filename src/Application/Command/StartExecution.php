<?php

namespace Util\Orchestrator\Application\Command;

use InvalidArgumentException;
use function is_array;
use function is_scalar;

class StartExecution
{
    /**
     * @var string
     */
    private $executionId;

    /**
     * @var string
     */
    private $workflowName;

    /**
     * @var array
     */
    private $input;

    public function __construct(string $executionId, string $workflowName, ...$input)
    {
        $this->setExecutionId($executionId);
        $this->setWorkflowName($workflowName);
        $this->setInput($input);
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function workflowName(): string
    {
        return $this->workflowName;
    }

    public function input(): array
    {
        return $this->input;
    }

    private function setExecutionId(string $executionId): void
    {
        if ('' === $executionId) {
            throw new InvalidArgumentException('ExecutionId may not be empty');
        }
        $this->executionId = $executionId;
    }

    private function setWorkflowName(string $workflowName): void
    {
        if ('' === $workflowName) {
            throw new InvalidArgumentException('WorkflowName may not be empty');
        }
        $this->workflowName = $workflowName;
    }

    private function setInput(array $input): void
    {
        $this->ensureIsValidInput($input);
        $this->input = $input;
    }

    private function ensureIsValidInput($input): void
    {
        foreach ($input as $value) {
            $isArray = is_array($value);
            if (!is_scalar($value) && !$isArray) {
                throw new InvalidArgumentException('Input may contains scalar value or array');
            }
            if ($isArray) {
                $this->ensureIsValidInput($value);
            }
        }
    }
}
