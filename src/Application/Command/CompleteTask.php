<?php

namespace Util\Orchestrator\Application\Command;

use InvalidArgumentException;

class CompleteTask
{
    /**
     * @var string
     */
    private $executionId;

    /**
     * @var int
     */
    private $taskId;

    /**
     * @var string
     */
    private $result;

    public function __construct(string $executionId, int $taskId, string $result)
    {
        $this->setExecutionId($executionId);
        $this->setTaskId($taskId);
        $this->setResult($result);
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function taskId(): int
    {
        return $this->taskId;
    }

    public function result(): string
    {
        return $this->result;
    }

    private function setExecutionId(string $executionId): void
    {
        if ('' === $executionId) {
            throw new InvalidArgumentException('ExecutionId may not be empty');
        }
        $this->executionId = $executionId;
    }

    private function setTaskId(int $taskId): void
    {
        if ($taskId < 0) {
            throw new InvalidArgumentException('TaskId may not be less than zero');
        }
        $this->taskId = $taskId;
    }

    private function setResult(string $result): void
    {
        $this->result = $result;
    }
}
