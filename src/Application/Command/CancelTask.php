<?php

namespace Util\Orchestrator\Application\Command;

use InvalidArgumentException;

class CancelTask
{
    /**
     * @var string
     */
    private $executionId;

    /**
     * @var int
     */
    private $taskId;

    /**
     * @var string
     */
    private $cancellation;

    public function __construct(string $executionId, int $taskId, string $cancellation)
    {
        $this->setExecutionId($executionId);
        $this->setTaskId($taskId);
        $this->setCancellation($cancellation);
    }

    public function executionId(): string
    {
        return $this->executionId;
    }

    public function taskId(): int
    {
        return $this->taskId;
    }

    public function cancellation(): string
    {
        return $this->cancellation;
    }

    private function setExecutionId(string $executionId): void
    {
        if ('' === $executionId) {
            throw new InvalidArgumentException('ExecutionId may not be empty');
        }
        $this->executionId = $executionId;
    }

    private function setTaskId(int $taskId): void
    {
        if ($taskId < 0) {
            throw new InvalidArgumentException('TaskId may not be less than 0');
        }
        $this->taskId = $taskId;
    }

    private function setCancellation(string $cancellation): void
    {
        $this->cancellation = $cancellation;
    }
}
