<?php

namespace Util\Orchestrator\Application;

use Util\Orchestrator\Application\Command\CancelTask;
use Util\Orchestrator\Application\Command\CompleteTask;
use Util\Orchestrator\Application\Command\FailTask;
use Util\Orchestrator\Application\Command\StartExecution;
use Util\Orchestrator\Domain\DurableContext;
use Util\Orchestrator\Domain\ExecutionId;
use Util\Orchestrator\Domain\Workflow;
use Util\Pubsub\EventPublisher;
use DomainException;
use Throwable;
use function unserialize;

class Orchestrator
{
    private $eventStore;

    private $eventPublisher;

    private $workflow;

    public function __construct(
        EventStore $eventStore,
        EventPublisher $eventPublisher,
        Workflow $workflow
    ) {
        $this->eventStore = $eventStore;
        $this->eventPublisher = $eventPublisher;
        $this->workflow = $workflow;
    }

    public function startExecution(StartExecution $command)
    {
        $this->orchestrate(
            new ExecutionId($command->executionId()),
            function (DurableContext $context) use ($command): void {
                $context->startExecution($command->workflowName(), $command->input());
            }
        );
    }

    public function completeTask(CompleteTask $command)
    {
        $this->orchestrate(
            new ExecutionId($command->executionId()),
            function (DurableContext $context) use ($command): void {
                $context->completeTask($command->taskId(), $command->result());
            }
        );
    }

    public function failTask(FailTask $command)
    {
        $this->orchestrate(
            new ExecutionId($command->executionId()),
            function (DurableContext $context) use ($command): void {
                $context->failTask($command->taskId(), $command->exception());
            }
        );
    }

    public function cancelTask(CancelTask $command)
    {
        $this->orchestrate(
            new ExecutionId($command->executionId()),
            function (DurableContext $context) use ($command): void {
                $context->cancelTask($command->taskId(), $command->cancellation());
            }
        );
    }

    private function orchestrate(ExecutionId $executionId, callable $routine): void
    {
        $context = new DurableContext($executionId, $this->eventStore->load($executionId->id()));
        $routine($context);
        $this->runOrchestration($context, ...$context->input());
        $changes = $context->changes();
        $this->eventStore->append($executionId->id(), $changes);
        $this->eventPublisher->publish(...$changes);
    }

    private function runOrchestration(DurableContext $context, ...$input): void
    {
        try {
            $sequenceFlow = $this->workflow->run($context, ...$input);
            while ($sequenceFlow->valid()) {
                $task = $context->task($sequenceFlow->current());
                if ($task->isScheduled()) {
                    break;
                } elseif ($task->isFaulted()) {
                    throw new DomainException($task->exception());
                } else {
                    if ('whenAll' === $task->operation()
                        || 'whenAny' === $task->operation()
                    ) {
                        $sequenceFlow->send(unserialize($task->result()));
                    } else {
                        $sequenceFlow->send($task->result());
                    }
                }
            }
            if (!$sequenceFlow->valid()) {
                $context->completeExecution();
            }
        } catch (Throwable $exception) {
            $context->failExecution(
                $exception->getMessage()
                . ' - ' . basename($exception->getFile())
                . ':' . $exception->getLine()
            );
        }
    }
}
